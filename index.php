<?php
	date_default_timezone_set('America/Sao_Paulo');
	$hora= date("H");
	if($hora<12){
	   $saudacao = "Bom dia!";
	}else if($hora<18){
	   $saudacao="Boa tarde!";
	}else{
	   $saudacao = "Boa noite!";
	}

	session_start();
	$logado = isset($_SESSION['usuario']);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>BAZAR DOS MANOS</title>
		<meta charset="UTF-8"/>
		<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>
	<body>
		<?php
			require_once("header.php");
		?>
		<div class="container">
			<br>
			<span><?php echo $saudacao?></span>
			<br><br>
			<span>Faça login para ver nossos produtos!</span>
			<?php
				if ($logado) {
					Header("location:loja.php");
				} else {
					require_once("login.php");
				}
			?>
		</div>
		<?php
			require_once("footer.php");
		?>
	</body>
</html>
